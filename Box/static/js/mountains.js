var MountainConfig = {
	screenHeight: 250,
	screenWidth: 1000,
	maxLevel: 4
};

// We create a constant mountain with all zeroes to help the GC when running update().
const zeroMountain = {x: 0, y: 0, width: 0};

var MountainRange = function (context, level) {
	console.assert(level < MountainConfig.maxLevel);

	context.canvas.width = MountainConfig.screenWidth;
	context.canvas.height = MountainConfig.screenHeight;

	const modifier = MountainConfig.maxLevel - level;
	const minWidths = [110, 90, 70, 40];
	const maxWidths = [180, 150, 120, 70];

	this.width    = 0;
	this.base     = 6 + 18 * (level + 1);
	this.height   = 30 + 30 * (level + 1);
	this.minWidth = minWidths[level]
	this.maxWidth = maxWidths[level]
	this.color    = 'hsl(120, ' + (modifier * modifier + 10) + '%, ' + (75 - modifier * 13) + '%)';
	this.offset   = 0;
	this.range    = [];
	this.context  = context;

	this.update(0);
};

MountainRange.prototype.update = function (offset) {
	this.offset += offset;

	// Remove mountains that won't be seen.
	var deleteCount = 0;
	for (var i = 0; i < this.range.length; i++) {
		const mountain = this.range[i];
		// If the mountain we're looking at isn't off the left side of the screen,
		// we know it and the rest of the mountains are needed for drawing.
		if (mountain.x + mountain.width - this.offset >= -2 * this.maxWidth) {
			break;
		}

		deleteCount++;
		this.width -= mountain.width;
	}
	if (deleteCount > 0) {
		this.range.splice(0, deleteCount);
	}

	var lastMountain = zeroMountain;
	if (this.range.length == 0) {
		// If there are no mountains, we should reset the offset to avoid the case where
		// we simply purged all the mountains and are starting over.
		this.offset = 0;
	}
	else {
		// If there are mountains, we should build off of the last one.
		lastMountain = this.range[this.range.length - 1];
	}

	// Add enough mountains to fill the screen, plus a bit more to keep the right edge interesting.
	while (this.width < MountainConfig.screenWidth + 4 * this.maxWidth) {
		var width = randomInt(this.minWidth, this.maxWidth);
		var height = randomInt(this.base, this.base + this.height);
		const mountain = {
			x: lastMountain.x + lastMountain.width,
			y: MountainConfig.screenHeight - height,
			width: width,
		};

		this.range.push(mountain);
		this.width += mountain.width;
		lastMountain = mountain;
	}
};

MountainRange.prototype.render = function () {
	const startX = this.range[0].x - this.range[0].width - this.offset;
	// Erase the previous scene
	this.context.clearRect(0, MountainConfig.screenHeight - (this.base + this.height), MountainConfig.screenWidth, MountainConfig.screenHeight);

	// Connect the base level on the left side of the screen to the mountain range
	this.context.beginPath();
	this.context.moveTo(startX, MountainConfig.screenHeight);
	this.context.lineTo(startX, MountainConfig.screenHeight - this.base);

	// Draw each mountain using quadratic curves and taking the average of adjacent mountains as their midpoint.
	const length = this.range.length;
	for (var i = 0; i < length - 1; i++) {
		const mountain = this.range[i];
		const nextMountain = this.range[i+1];

		const endX = ((mountain.x + nextMountain.x) / 2) - this.offset;
		const endY = (mountain.y + nextMountain.y) / 2;

		this.context.quadraticCurveTo(mountain.x - this.offset, mountain.y, endX, endY);

		if (endX > MountainConfig.screenWidth) {
			break;
		}
	}

	// Give the mountains a slight outline.  We make sure to stroke before filling because we don't
	// want to put a line on the bottom of the canvas.
	this.context.strokeStyle = "#000";
	this.context.lineWidth = 1;
	this.context.stroke();

	// Connect the right side of the mountain range to the ground
	const lastMountain = this.range[length - 1];
	this.context.lineTo(lastMountain.x - this.offset + lastMountain.width, MountainConfig.screenHeight);
	this.context.closePath();

	// Color the mountain range with a shade of green
	this.context.fillStyle = this.color;
	this.context.fill();
};

function randomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

function initializeMountains(canvasName) {
	let mountains = [];

	for (let i = 0; i < MountainConfig.maxLevel; i++) {
		let canvas = document.getElementById(canvasName + (i + 1));
		mountains.push(new MountainRange(canvas.getContext('2d'), i));
	}

	var maxLayer = 0;
	var lastTime = performance.now();

	function render(timestamp) {
		// Render the scene, updating the position of the mountains to shift them left as time passes.
		// As an optimization, we draw each mountain range on its own canvas and only update them
		// when necessary.  This means we'll redraw the front range that moves the fastest every frame,
		// but will only redraw the slow-moving back range every 4 frames.

		// Determine how much time has passed
		const steps = (timestamp - lastTime) / 50;

		for (var i = 0; i <= maxLayer; i++) {
			mountains[i].render();
			mountains[i].update(steps);
		}

		// Update the per-frame information
		maxLayer = (maxLayer + 1) % mountains.length;
		lastTime = timestamp;

		requestAnimationFrame(render);
	}

	requestAnimationFrame(render);
}
