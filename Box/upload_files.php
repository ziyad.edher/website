<?php

if (isset($_FILES['files'])){
	$basedir = "/home/mbryant/public_html/Box/uploads/";

	foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
		$errors = array();
		$file_name = basename($_FILES['files']['name'][$key]);
		$file_size = $_FILES['files']['size'][$key];
		$file_tmp = $_FILES['files']['tmp_name'][$key];

		switch ($_FILES['files']['error'][$key]) {
			case UPLOAD_ERR_OK: // No error uploading
				break;
            case UPLOAD_ERR_INI_SIZE:
                $error[] = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $error[] = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $error[] = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $error[] = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $error[] = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $error[] = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $error[] = "File upload stopped by extension";
                break;
            default:
                $error[] = "Unknown upload error";
                break;
		}

		if (!preg_match("`^[-0-9A-Za-z_\.]+$`i",$file_name)) {
			$errors[] = "Filenames can only contain alphanumeric characters, periods, and underscores, and cannot include spaces.";
		}
		if ($file_size > 300000000){
			$errors[] = "File size must be less than 256 MB";
        }
        if(empty($errors)) {
	 		move_uploaded_file($file_tmp, $basedir.$file_name) or die("Failure writing $file_name to disk.");
		}
		else {
			http_response_code(406);

			echo "The following errors occured while uploading the file <b>".$file_name."</b>:";
			echo "<ol>";
			foreach ($errors as $line)
				echo "<li>$line</li>";
			echo "</ol>";
			return;
		}
    }
}
?>
