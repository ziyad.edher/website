///////////////////////////////////////////////////////////////////////////////
// Globals
///////////////////////////////////////////////////////////////////////////////
let UI = {
	// List sidebar
	list         : document.getElementById("list"),

	// Splash page
	splash       : document.getElementById("splash"),

	// Recipe region
	recipe       : document.getElementById("recipe"),
	name         : document.getElementById("recipe-name"),
	categories   : document.getElementById("recipe-categories"),
	ingredients  : document.getElementById("recipe-ingredients"),
	instructions : document.getElementById("recipe-instructions"),
	notes        : document.getElementById("recipe-notes"),
	notesHead    : document.getElementById("recipe-notes-head"),

	// Creation page
	create             : document.getElementById("creation"),
};

let State = {
	focused : UI.splash,

	data   : [],
	list   : new SidebarList(UI.list, renderRecipe),
};

function onload(recipes) {
	State.data = recipes;

	// Sort the recipes before adding them to the list.
	// TODO: This should be done with a category system in the future.
	State.data.sort((a, b) => {
		if (a.name < b.name) {
			return -1;
		}
		if (a.name > b.name) {
			return 1;
		}

		return 0;
	});

	// Initialize recipes
	for (let i in State.data) {
		let recipe = State.data[i];

		let id = State.list.addElement({
			text:     recipe.name,
			contents: recipe,
		});

		recipe.currentStep = -1;
		recipe.id          = id;

		// Parse the number of instructions that depend on this ingredient
		for (let i in recipe.instructions) {
			const instruction = recipe.instructions[i];

			if (instruction.ingredients === undefined) {
				continue;
			}

			for (let j in instruction.ingredients) {
				let name = instruction.ingredients[j];

				if (recipe.ingredients[name] === undefined) {
					console.error(`Ingredient ${name} not found`);
					continue;
				}
				if (recipe.ingredients[name].uses === undefined) {
					// Track the number of times this ingredient needs to be
					// used separately from the number of times it has been
					// used, to allow for restarting the recipe.
					recipe.ingredients[name].uses = 0;
					recipe.ingredients[name].used = 0;
				}

				recipe.ingredients[name].uses++;
			}
		}
	}

	// Render the list now that it has been populated.
	State.list.render();
}

// TODO: recipe should be a class
function advanceStep(recipe) {
	if (recipe.currentStep !== -1) {
		// We're currently in progress. Mark that this step has been completed.
		let instruction = recipe.instructions[recipe.currentStep];

		// Mark that we consumed our ingredients
		for (let i in instruction.ingredients) {
			let next = recipe.ingredients[instruction.ingredients[i]];

			next.used++;
			next.li.classList.remove("ingredient-next");

			// TODO: Should we be treating ingredients as classes?
			// It would be quite nice to abstract this class code out
			// via getters and setters.
			if (next.used === next.uses) {
				next.li.classList.add("ingredient-consumed");
			}
		}
	}

	recipe.currentStep++;

	if (recipe.currentStep < recipe.instructions.length) {
		// If there's still a next instruction, bold it and the
		// ingredients it'll use.
		const next = recipe.instructions[recipe.currentStep];

		next.div.classList.add("instruction-next");
		for (let i in next.ingredients) {
			recipe.ingredients[next.ingredients[i]].li.classList.add("ingredient-next");
		}

		recipe.listElement.classList.add("recipe-item-active");
	} else {
		// We're at the end, so the recipe is now completed.
		recipe.listElement.classList.remove("recipe-item-active");
		recipe.listElement.classList.add("recipe-item-completed");
	}
}

function renderRecipe(recipe) {
	if (State.focused != UI.recipe) {
		// This is the first recipe we've displayed, so clear the displayed
		// page and show the recipe section instead.
		State.focused.classList.remove("main-selected");
		UI.recipe.classList.add("main-selected");
		State.focused = UI.recipe;
	}

	document.title = "Recipes - " + recipe.name;
	UI.name.innerText = recipe.name;
	UI.categories.innerText = recipe.categories.reduce((a,b) => a + ", " +  b);

	UI.ingredients.innerHTML = "";
	for (let ingredientName in recipe.ingredients) {
		let ingredient = recipe.ingredients[ingredientName];
		let li = document.createElement("div");

		ingredient.li = li;

		let amount = document.createElement("span");
		amount.classList.add("ingredients-amount");
		// Replace fractions with fancy unicode.
		amount.innerText = ingredient.amount
			.replace("1/2", "½")
			.replace("1/3", "⅓").replace("2/3", "⅔")
			.replace("1/4", "¼").replace("3/4", "¾")
			.replace("1/8", "⅛").replace("3/8", "⅜").replace("5/8", "⅝").replace("7/8", "⅞");
		li.appendChild(amount);

		let name = document.createElement("span");
		name.classList.add("ingredients-name");
		name.innerText = ingredientName;
		li.appendChild(name);

		if ((ingredient.modifier !== undefined) && (ingredient.modifier !== "")) {
			let modifier = document.createElement("span");
			modifier.classList.add("ingredients-modifier");
			modifier.innerText = ingredient.modifier;
			li.appendChild(modifier);
		}

		if (ingredient.used === ingredient.uses) {
			// If we've consumed this ingredient already, mark it as such.
			li.classList.add("ingredient-consumed");
		}

		UI.ingredients.appendChild(li);
	}

	UI.notes.innerHTML = "";
	UI.notesHead.classList.toggle("hidden", recipe.notes.length == 0);
	for (let i in recipe.notes) {
		// Briefly sanitize the note, though we trust our inputs.
		let li = document.createElement("li");
		li.innerText = recipe.notes[i];

		// Sanitize the note, then find links to any other recipes and expand them.
		const note = li.innerText;
		if (note.includes("https://recipes.programsareproofs.com/#")) {
			li.innerHTML = note.replace(/https:\/\/recipes.programsareproofs.com\/#([a-zA-Z_]+)/g, (match, p1) =>
				`<a href="${match}">${p1.replace(/_/g, " ")}</a>`
			);
		}

		UI.notes.appendChild(li);
	}

	UI.instructions.innerHTML = "";

	if (recipe.currentStep == -1) {
		// We haven't started the recipe yet, so show the start button.
		let div = document.createElement("div");

		div.className = "start-button";
		div.addEventListener("click",  () => {
			advanceStep(recipe);
			div.onclick = null;
			UI.instructions.removeChild(div);
		});

		UI.instructions.appendChild(div);
	}

	for (let step = 0; step < recipe.instructions.length; step++) {
		let instruction = recipe.instructions[step];
		let div = document.createElement("div");
		instruction.div = div;

		div.innerText = instruction.step;
		div.innerHTML = div.innerText.replace(/ degrees/g, "&#x2109");
		div.onclick = () => {
			if (recipe.currentStep !== step) {
				// If we aren't the current instruction, we can't be completed.
				return;
			}

			advanceStep(recipe);
			instruction.div.classList.remove("instruction-next");
			instruction.div.classList.add("instruction-complete");
			instruction.div.onclick = null;
		};

		if (step < recipe.currentStep) {
			// We've previously completed this step, mark it as completed.
			div.classList.add("instruction-complete");
		} else if (step === recipe.currentStep) {
			// Bold the next instruction and ingredients it requires
			div.classList.add("instruction-next");

			for (let i in instruction.ingredients) {
				recipe.ingredients[instruction.ingredients[i]].li.classList.add("ingredient-next");
			}
		}

		UI.instructions.appendChild(div);
	}

	// If this is the first result we've clicked on after searching, hide the
	// search page.
	// TODO: better way?
	if (UI.recipe.style.display !== "block") {
		UI.recipe.classList.add("viewing");
	}
}

let RecipeCreation = class {
	constructor() {
		// Fetch necessary UI components
		this.UI = {
			begin        : document.getElementById("creation-button"),
			title        : document.getElementById("creation-title"),
			categories   : document.getElementById("creation-categories"),
			ingredients  : document.getElementById("creation-ingredients"),
			instructions : document.getElementById("creation-instructions"),
			notes        : document.getElementById("creation-notes"),
			submit       : document.getElementById("creation-generate"),
			error        : document.getElementById("creation-error"),
			result       : document.getElementById("creation-result"),
		};

		// Initialize the default form
		this.newIngredient();
		this.newInstruction();
		this.newNote();

		// Handle the recipe creation portion
		this.UI.begin.addEventListener("click", () => this.display());

		// Handle the submit button
		this.UI.submit.addEventListener("click", () => this.generate());

		// TODO: have a way to infer/specify ingredients
	}

	newIngredient() {
		let empty = true;
		let div   = document.createElement("div");
		div.innerHTML = "<input type='text' name='ingredient' placeholder='Ingredient'><input type='text' name='amount' placeholder='Amount'>(<input type='text' name='modifier' placeholder='Modifier'>)";
		div.classList.add("create-ingredient");
		let inputs = Array.from(div.getElementsByTagName("input"));

		inputs.map(x => x.addEventListener("input", () => {
			// Ensure we have exactly one empty ingredient at the end.
			if (inputs.some(x => x.value !== "")) {
				// This is a non-empty ingredient.
				if (empty) {
					// We've made this a non-empty ingredient, so create a new
					// one.
					this.newIngredient();
				}
				empty = false;
			} else {
				// We cleared all fields of this ingredient, so remove it.
				// Reset focus to the last ingredient.
				let parent = div.parentElement;

				parent.removeChild(div);
				parent.lastChild.firstChild.focus();
			}
		}));

		this.UI.ingredients.appendChild(div);
	}

	newInstruction() {
		let empty  = true;
		let input  = document.createElement("input");
		input.type = "text";
		input.placeholder = "Step";
		input.classList.add("create-instruction");

		input.addEventListener("input", () => {
			// Ensure we have exactly one empty instruction at the end.
			if (input.value === "") {
				// We cleared this note, so remove it.
				// First ensure we don't lose focus.
				if (input.nextSibling !== null) {
					input.nextSibling.focus();
				} else {
					input.previousSibling.focus();
				}

				input.parentElement.removeChild(input);
			} else {
				if (empty) {
					// We've made this a non-empty note, so create a new one.
					this.newInstruction();
				}
				empty = false;
			}
		});

		this.UI.instructions.appendChild(input);
	}

	newNote() {
		let empty  = true;
		let li     = document.createElement("li");
		let input  = document.createElement("input");
		input.type = "text";
		input.placeholder = "Note";
		input.classList.add("create-note");

		input.addEventListener("input", () => {
			// Ensure we have exactly one empty note at the end.
			// TODO: These event listeners feel like we could use a generic
			// function.
			if (input.value === "") {
				// We cleared this note, so remove it.
				// First ensure we don't lose focus.
				if (li.nextSibling !== null) {
					li.nextSibling.firstChild.focus();
				} else {
					li.previousSibling.firstChild.focus();
				}

				input.parent.removeChild(li);
			} else {
				if (empty) {
					// We've made this a non-empty note, so create a new one.
					this.newNote();
				}
				empty = false;
			}
		});

		li.appendChild(input);
		this.UI.notes.appendChild(li);
	}

	generate() {
		let title = this.UI.title.value.trim();
		if (title === "") {
			this.error("No valid title entered", this.UI.title);
			return;
		}

		let categories = this.UI.categories.value.split(",").map(x => x.trim()).filter(x => x.length > 0);
		if (categories.length === 0) {
			this.error("No valid categories entered", this.UI.categories);
			return;
		}

		// Iterate through all ingredients but the last one, as it is always empty.
		let ingredients = {};
		for (let div of Array.from(this.UI.ingredients.children).slice(0, -1)) {
			let inputs = div.getElementsByTagName("input");
			let name   = inputs[0].value.trim();

			let data = {
				amount: inputs[1].value.trim(),
				modifier: inputs[2].value.trim(),
			};

			if (name === "") {
				this.error("No name provided for ingredient", inputs[0]);
				return;
			}
			if ((data.modifier === "") && (data.amount == "")) {
				this.error(`Ingredient "${name}" needs either an amount or a modifier.`, inputs[0]);
				return;
			}

			if (name in ingredients) {
				this.error(`Duplicated ingredient ${name}`, inputs[0]);
				return;
			}

			ingredients[name] = data;
		};
		if (Object.keys(ingredients).length === 0) {
			this.error("No valid ingredients entered", this.UI.ingredients.getElementsByTagName("input")[0]);
			return;
		}

		let instructions = Array.from(this.UI.instructions.children).map(input => input.value.trim()).filter(x => x.length > 0);
		if (instructions.length === 0) {
			this.error("No valid instructions entered", this.UI.instructions.children[0]);
			return;
		}

		// There's no such thing as an invalid note
		let notes = Array.from(this.UI.notes.children).map(li => li.firstChild.value.trim()).filter(x => x.length > 0);

		// Entry was successful.
		// Hide the error, if one had previously been generated.
		this.UI.error.classList.add("hidden");
		this.UI.submit.classList.add("hidden");

		// Generate the proper recipe format, and display it in place of the submit button.
		let recipe = {
			name: title,
			categories: categories,
			ingredients: ingredients,
			instructions: instructions.map(step => { return {
				step: step,
				ingredients: [],
			}}),
			notes: notes
		};

		// Display the recipe in place of the submit button
		let result = this.UI.result.getElementsByTagName("pre")[0];
		result.innerText = JSON.stringify(recipe, null, 4);
		this.UI.result.classList.remove("hidden");
	}

	display() {
		State.focused.classList.remove("main-selected");
		UI.create.classList.add("main-selected");
		State.focused = UI.create;
	}

	error(s, node) {
		this.UI.error.innerText = s;
		this.UI.error.classList.remove("hidden");
		node.focus();
	}
}

let create = new RecipeCreation();

let initialize = function() {
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "data.json", true);
	xhr.responseType = "json";
	xhr.addEventListener("loadend", () => {
		if ((xhr.response === null) || (Object.keys(xhr.response).length == 0)) {
			alert("Failed to fetch recipes!");
			return;
		} else {
			onload(xhr.response);
		}
	});
	xhr.send();
}();
