// Construct an empty list in the node that contains the script
// that initialized this list by including the file.
var ShoppingList = new function () {
	var items = [];
	var node = document.scripts[document.scripts.length - 1].parentNode;

	var saveItems = function() {
		postReq("save.php")(items)();
	};

	// Load the list of items
	var loadItems = function(first) {
		first = (typeof(first) === "undefined") ? "" : "?first";

		postReq("load.php" + first, "GET")()(function(json) {
			items = JSON.parse(json);
			render();

			loadItems();
		}, loadItems);
	};

	this.addItem = function(form) {
		value = form.value.trim();
		form.value = "";

		if (value == "") {
			return;
		}
		if (value.indexOf("<") != -1 || value.indexOf(">") != -1) {
			alert("No HTML tags!");  // We never render HTML from user input anyways
			return;
		}

		items.push({"name": value, "done":false});
		saveItems();
	};

	var deleteItem = function(id) {
		return (function() {
			// Remove the node from our local copy then propagate the change.
			items.splice(id, 1);
			var elem = node.childNodes[id];

			saveItems();
		}).bind(this);
	};

	var postReq = function(url, type) {
		if (typeof(type) === 'undefined') {
			type = "POST";
		}
		var req = new XMLHttpRequest();

		req.open(type, url, true);
		if (type === "POST") {
			req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		}

		return (function(params) {
			return (function(load, timeout) {
				req.ontimeout = function() {
					if (typeof(timeout) === "undefined") {
						return;
					}
					timeout.call(this);
				};
				req.onload = (function() {
					if (typeof(load) === "undefined") {
						return;
					}
					if (req.status == 200) {
						load.call(this, req.responseText);
					}
					else if (req.status == 504) {
						if (typeof(timeout) === "undefined") {
							return;
						}
						timeout.call(this);
					}
				}).bind(this);

				req.send(type == "POST" ? "json=" + JSON.stringify(params) : "");
			}).bind(this);
		}).bind(this);
	};

	var render = function() {
		node.innerHTML = "";

		for (var i = 0; i < items.length; i++) {
			var checkbox = document.createElement("input");
			checkbox.type = "checkbox";
			checkbox.onchange = (function(i, checkbox) {
				items[i].done = checkbox.checked;
				render();
				saveItems();
			}).bind(this, i, checkbox);
			checkbox.checked = items[i].done;

			var label = document.createElement("label");
			label.textContent = items[i].name;
			if (items[i].done) {
				label.style.textDecoration = "line-through";
			}

			var remove = document.createElement("button");
			remove.onclick = deleteItem(i);

			label.insertBefore(checkbox, label.firstChild);
			label.appendChild(remove);
			node.appendChild(label);
		}
	};

	// Load the initial page as soon as possible
	loadItems(true);
};
