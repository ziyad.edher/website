// Object deep-copy
Object.prototype.clone = function() {
	var newObj = (this instanceof Array) ? [] : {};
	for (i in this) {
		if (i == 'clone')
			continue;
		if (this[i] && typeof this[i] == "object")
			newObj[i] = this[i].clone();
		else newObj[i] = this[i]
	}
	return newObj;
};

// Random Number class
var Random = new function () {
	// Return an int within [min, max]
	this.nextInt = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};
	// Return a float within [min, max]
	this.nextFloat = function(min, max) {
		return Math.random() * (max - min + 1) + min;
	};
	// Return a bool with p probability of being true
	this.nextBool = function(p) {
		return Math.random() < p;
	};
};

// Balls class
var Balls = new function () {

	// Set up variables
	this.lCanvas = this.rCanvas = this.canvasWidth = this.canvasHeight = this.canvasPercent = this.lContext = this.rContext = this.numBalls = this.ballWorker = null;
	this.ball = [];

	// Clear the two canvases
	this.clearCanvases = function() {
		Balls.lContext.clearRect(0, 0, Balls.canvasWidth, Balls.canvasHeight);
		Balls.rContext.clearRect(0, 0, Balls.canvasWidth, Balls.canvasHeight);
	};

	// Grab screen sizing and set canvas sizes
	this.setGlobalSizing = function () {
		Balls.canvasWidth = document.body.clientWidth / (100 / Balls.canvasPercent);
		Balls.canvasHeight = document.body.clientHeight;
		Balls.lCanvas.height = Balls.canvasHeight;
		Balls.rCanvas.height = Balls.canvasHeight;
		Balls.lCanvas.width = Balls.canvasWidth;
		Balls.rCanvas.width = Balls.canvasWidth;

		// Send message on update if necessary
		if (Balls.ballWorker != null) {
			Balls.ballWorker.postMessage("size"+JSON.stringify([Balls.canvasWidth, Balls.canvasHeight]));
		}
	};

	// Initial ball constructer
	this.initBalls = function (numBalls, canvasPercent) {
		Balls.canvasPercent = canvasPercent;
		Balls.lCanvas = document.getElementById('lCanvas');
		Balls.rCanvas = document.getElementById('rCanvas');
		if (!Balls.lCanvas || !Balls.rCanvas || Balls.lCanvas.style.display == "none" || Balls.rCanvas.style.display == "none")
			return false;
		// Attach click event listeners once canvases are active
		Balls.lCanvas.addEventListener('click', function(event){Balls.pop(event, Balls.ball.clone());}, false);
		Balls.rCanvas.addEventListener('click', function(event){Balls.pop(event, Balls.ball.clone());}, false);
		if (Balls.lCanvas.getContext) {
			Balls.lContext = Balls.lCanvas.getContext('2d');
			Balls.rContext = Balls.rCanvas.getContext('2d');
			Balls.lContext.lineWidth = Balls.rContext.lineWidth = 4;
			Balls.lContext.strokeStyle = Balls.rContext.strokeStyle = '#000055';
			Balls.setGlobalSizing();
			for (var i = 0; i < numBalls; i++) {
				Balls.ball[i] = {x:Random.nextInt(20, Balls.canvasWidth*2), y:Random.nextInt(20, Balls.canvasHeight), radius: Random.nextInt(30,60), xVel:Random.nextFloat(-3,3), yVel:Random.nextFloat(-3,3)};
			}
			Balls.numBalls = numBalls;
			// Initialize the webworker if possible
			if (typeof(Worker) !== "undefined") {
				Balls.ballWorker = new Worker("ballWorker.js?version=" + Date.now());
				Balls.ballWorker.onmessage = function(event) {
					var json = JSON.parse(event.data);
					Balls.ball = json[0];
					Balls.numBalls = json[1];
				}
				// Send initial data and start the worker
				try {
					seen = []
						Balls.ballWorker.postMessage("data"+ JSON.stringify(Balls,
							// Remove circular dom references before sending
							function(key, val) {
								if (typeof val == "object") {
									if (seen.indexOf(val) >= 0)
										return undefined;
									seen.push(val);
								}
								return val;
							}
						));
					seen = null;
					Balls.ballWorker.postMessage("init"+JSON.stringify(0));
					if (window.requestAnimationFrame) {
						requestAnimationFrame(Balls.drawBalls);
					}
					else {
						setInterval(Balls.drawBalls, 30);
					}
					console.log("You are using a modern browser!");
				}
				catch (err) {
					Balls.ballWorker = null;
				}
			}
			if (Balls.ballWorker == null)
				setInterval(Balls.moveBalls, 26);
		}
	};

	// Calculate balls and draw them
	this.moveBalls = function () {
		for (var i = Balls.numBalls - 1; i >= 0; i--)
			Balls.calcBall(Balls.ball[i]);

		// Spawn a new ball if there aren't very many
		if (Balls.numBalls < 10 && !Random.nextInt(0, 100)) {
			Balls.ball[Balls.numBalls++] = {x:Random.nextInt(20, Balls.canvasWidth*2), y:Random.nextInt(20, Balls.canvasHeight), radius: Random.nextInt(30,60), xVel:Random.nextFloat(-3,3), yVel:Random.nextFloat(-3,3)};
		}

		Balls.drawBalls();
	};

	// Calculate the new ball positions
	this.calcBall = function(ball) {
		// Randomly angle ball
		if (Random.nextBool(.1)) {
			ball.yVel += Random.nextInt(-1, 1);
			ball.xVel += Random.nextInt(-1, 1);
			if (ball.xVel > 3 || ball.xVel < -3)
				ball.xVel /= 2;
			if (ball.yVel > 3 || ball.yVel < -3)
				ball.yVel /= 2;
		}

		// Check bounding box of window
		if (ball.y + ball.radius > Balls.canvasHeight && ball.yVel >= 0)
			ball.yVel *= -1;
		else if (ball.y - ball.radius < 0 && ball.yVel <= 0)
			ball.yVel *= -1;
		if (ball.x + ball.radius > Balls.canvasWidth*2 && ball.xVel >= 0)
			ball.xVel *= -1;
		else if (ball.x - ball.radius < 0 && ball.xVel <= 0)
			ball.xVel *= -1;
		ball.x += ball.xVel;
		ball.y += ball.yVel;
	};

	// Draw balls
	this.drawBalls = function() {
		requestAnimationFrame(Balls.drawBalls);
		Balls.clearCanvases();

		// Draw the circles.
		for (var i = Balls.numBalls - 1; i >= 0; i--) {
			var ball = Balls.ball[i];

			x = Math.round(ball.x);
			y = Math.round(ball.y);
			if (ball.x + ball.radius > Balls.canvasWidth) {
				Balls.rContext.beginPath();
				Balls.rContext.arc(x - Balls.canvasWidth, y, ball.radius, 0, Math.PI * 2, false);
				Balls.rContext.stroke();
				Balls.rContext.closePath();
			}
			if (ball.x - ball.radius < Balls.canvasWidth) {
				Balls.lContext.beginPath();
				Balls.lContext.arc(x, y, ball.radius, 0, Math.PI * 2, false);
				Balls.lContext.stroke();
				Balls.lContext.closePath();
			}
		}
		// Clear the circle insides to make the bubbles merge
		for (var i = Balls.numBalls - 1; i >= 0; i--) {
			var ball = Balls.ball[i];

			x = Math.round(ball.x);
			y = Math.round(ball.y);
			if (ball.x + ball.radius > Balls.canvasWidth) {
				Balls.rContext.save();
				Balls.rContext.beginPath();
				Balls.rContext.arc(x - Balls.canvasWidth, y, ball.radius - 1, 0, Math.PI * 2, false);
				Balls.rContext.clip();
				Balls.rContext.clearRect(0, 0, Balls.canvasWidth, Balls.canvasHeight);
				Balls.rContext.closePath();
				Balls.rContext.restore();
			}
			if (ball.x - ball.radius < Balls.canvasWidth) {
				Balls.lContext.save();
				Balls.lContext.beginPath();
				Balls.lContext.arc(x, y, ball.radius - 1, 0, Math.PI * 2, false);
				Balls.lContext.clip();
				Balls.lContext.clearRect(0, 0, Balls.canvasWidth, Balls.canvasHeight);
				Balls.lContext.closePath();
				Balls.lContext.restore();
			}
		}
	};

	// Figure out which ball was clicked on and remove it
	this.pop = function(event, balls) {
		var numBalls = Balls.numBalls;
		var canvasWidth = Balls.canvasWidth;
		var x = event.clientX - ((event.clientX > canvasWidth) ? document.documentElement.clientWidth - 2 * canvasWidth: 0);
		var y = event.clientY;
		var ball = null;
		for (var i = 0; i < numBalls; i++) {
			ball = balls[i];
			if (Math.abs(ball.x - x) > ball.radius || Math.abs(ball.y - y) > ball.radius)
				continue;
			if (Math.sqrt(Math.pow(ball.x - x, 2) + Math.pow(ball.y - y, 2)) <= ball.radius) {
				Balls.ball.splice(i, 1);
				Balls.numBalls--;
				if (Balls.ballWorker != null)
					Balls.ballWorker.postMessage("drop"+JSON.stringify(i));
				break;
			}
		}
	};
}

// Reset variables on resize
window.addEventListener('resize', Balls.setGlobalSizing, false);
