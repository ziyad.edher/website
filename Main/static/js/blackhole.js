// Too many stars can be pretty slow on a mobile device.
const STAR_COUNT = 3000;
// Maximum star size in canvas pixels
const STAR_SIZE = 2;
// Ratio of the screen to the hole.
const HOLE_RATIO = 3.5;
// Number of radians to rotate per ms for stars closest to the hole.
const SPEED_MULTIPLIER = 0.0006;

// Particles Around the Parent
class Star {
    // Angle the star is located at around the hole
    angle = Math.random() * 2 * Math.PI;
    // Stars have a random opacity in [0.2, 1.0]
    opacity =  (8 * Math.random() + 2) / 10;
    // Pick a random size for the star.
    radius = Math.random() * STAR_SIZE;

    constructor(ctx, hole_radius) {
        this.ctx = ctx;
        // Distance the star is located from the hole's center.
        // The "brighter" the star is, the closer it is to the center.
        this.distance = (1 / this.opacity) * hole_radius;

        // Precompute how much we should be rotating per ms.
        // Stars which are further away will rotate much faster than nearby
        // stars.
        this.angleDelta = (1 / this.opacity) * SPEED_MULTIPLIER;
    }

    update(centerX, centerY, elapsedMs) {
        this.angle += this.angleDelta * elapsedMs;

        let x = centerX + this.distance * Math.cos(this.angle);
        let y = centerY + this.distance * Math.sin(this.angle);

        this.ctx.fillStyle = "rgba(255, 255, 255," + this.opacity + ")";
        this.ctx.beginPath();
        this.ctx.arc(x, y, this.radius, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.closePath();
    }
}


class BlackHole {
    constructor(canvas_name) {
        this.ctx = document.getElementById(canvas_name).getContext('2d');
        this.previousRender = null;

        // We want to remain centered even if the canvas is resized.
        addEventListener('resize', this.resizeCanvas.bind(this))
        this.resizeCanvas()

        // TODO: Hard to resize this.
        this.radius = Math.max(
			this.canvasWidth / 2 / HOLE_RATIO,
			this.canvasHeight / 2 / HOLE_RATIO
		);
        this.stars = Array.from({length: STAR_COUNT}, _ => new Star(this.ctx, this.radius));

        // Start drawing our black hole.
        requestAnimationFrame(this.render.bind(this));
    }

    resizeCanvas() {
        this.ctx.canvas.width = this.canvasWidth = window.innerWidth;
        this.ctx.canvas.height = this.canvasHeight = window.innerHeight;
    }

    render(renderTime) {
        // Track how long since we last rendered, to make sure we're smoothly
        // moving the stars.
        if (this.previousRender == null) {
            this.previousRender = renderTime;
        }
        let elapsed = renderTime - this.previousRender;
        this.previousRender = renderTime;

        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

        let centerX = this.canvasWidth / 2;
        let centerY = this.canvasHeight / 2;

        for (let i = this.stars.length; i > 0; i--) {
            this.stars[i-1].update(centerX, centerY, elapsed);
        }

        // Redraw the hole in the center.
        this.ctx.fillStyle = "#000";
        this.ctx.beginPath();
        this.ctx.arc(centerX, centerY, this.radius, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.closePath();

        requestAnimationFrame(this.render.bind(this));
    }
}

var hole = new BlackHole('black-hole');
