const MountainConfig = {
	screenHeight: 250,
	screenWidth: 1000,
	maxLevel: 4
};

// We create a constant mountain with all zeroes to help the GC when running update().
const zeroMountain = {x: 0, y: 0, width: 0};

class MountainRange {
	constructor(canvas, level) {
		// Higher level mountains are further in the back.
		console.assert(level < MountainConfig.maxLevel);

		///////////////////////////////////////////////////////////////////////
		// Mountain configuration
		///////////////////////////////////////////////////////////////////////
		// We'd like the mountains out front to be more mellow, so make them
		// much wider.
		const minWidths = [110, 90, 70, 40];
		const maxWidths = [180, 150, 120, 70];

		this.width    = 0;
		this.base     = 6 + 18 * (level + 1);
		this.height   = 30 + 30 * (level + 1);
		this.minWidth = minWidths[level]
		this.maxWidth = maxWidths[level]
		this.offset   = 0;
		this.range    = [];

		// Compute an initial set of mountains.
		this.update(0);

		///////////////////////////////////////////////////////////////////////
		// Canvas configuration
		///////////////////////////////////////////////////////////////////////
		let ctx = canvas.getContext('2d');
		ctx.canvas.width = MountainConfig.screenWidth;
		ctx.canvas.height = MountainConfig.screenHeight;

		// Performance optimization
		ctx.imageSmoothingEnabled = false;

		// Give the mountains a slight outline.
		ctx.strokeStyle = "#000";
		ctx.lineWidth = 1;

		// Color the mountain range with a shade of green.
		// NOTE: This equation must exactly match the colors defined in
		// `main.css` (and elsewhere across the site), so should not be
		// changed.
		const modifier = MountainConfig.maxLevel - level;
		ctx.fillStyle = 'hsl(120, ' + (modifier * modifier + 10) + '%, ' + (75 - modifier * 13) + '%)';

		// Shadow with our same color to act as blur above the mountains.
		// We exaggerate this more for further away mountains, giving the
		// effect of haze in the distance.
		ctx.shadowColor = ctx.fillStyle;
		ctx.shadowBlur = level * 2;
		ctx.shadowOffsetY = -1 * level;

		this.ctx = ctx;

		///////////////////////////////////////////////////////////////////////
		// Observability configuration
		///////////////////////////////////////////////////////////////////////
		// True iff this mountain range is visible on the screen.
		this.visible = true;

		new IntersectionObserver(elems => {
			// intersectionRatio is <= 0 if we're off-screen.
			this.visible = elems[0].intersectionRatio > 0;
		}).observe(canvas);
	};

	update(offset) {
		this.offset += offset;

		if (!this.visible) {
			// We've already updated the amount we've shifted, but any mountain
			// recomputation can wait for when we become visible again.
			return;
		}

		// Remove mountains that won't be seen.
		let deleteCount = 0;
		for (let i = 0; i < this.range.length; i++) {
			const mountain = this.range[i];
			// If the mountain we're looking at isn't off the left side of the screen,
			// we know it and the rest of the mountains are needed for drawing.
			if (mountain.x + mountain.width - this.offset >= -2 * this.maxWidth) {
				break;
			}

			deleteCount++;
			this.width -= mountain.width;
		}
		if (deleteCount > 0) {
			this.range.splice(0, deleteCount);
		}

		let lastMountain = zeroMountain;
		if (this.range.length == 0) {
			// If there are no mountains, we should reset the offset to avoid the case where
			// we simply purged all the mountains and are starting over.
			this.offset = 0;
		} else {
			// If there are mountains, we should build off of the last one.
			lastMountain = this.range[this.range.length - 1];
		}

		// Add enough mountains to fill the screen, plus a bit more to keep the right edge interesting.
		while (this.width < MountainConfig.screenWidth + 4 * this.maxWidth) {
			let width = randomInt(this.minWidth, this.maxWidth);
			let height = randomInt(this.base, this.base + this.height);
			const mountain = {
				x: lastMountain.x + lastMountain.width,
				y: MountainConfig.screenHeight - height,
				width: width,
			};

			this.range.push(mountain);
			this.width += mountain.width;
			lastMountain = mountain;
		}

		this.computed++;
	}

	render() {
		if (!this.visible) {
			// Don't waste energy re-rendering when it can't be seen.
			return;
		}

		const startX = this.range[0].x - this.range[0].width - this.offset;

		// Erase the previous scene.  We clear a bit extra to cover for any excess shadow.
		this.ctx.clearRect(0, MountainConfig.screenHeight - (this.base + this.height + 10), MountainConfig.screenWidth, MountainConfig.screenHeight);

		// Connect the base level on the left side of the screen to the mountain range
		this.ctx.beginPath();
		this.ctx.moveTo(startX, MountainConfig.screenHeight);
		this.ctx.lineTo(startX, MountainConfig.screenHeight - this.base);

		// Draw each mountain using quadratic curves and taking the average of adjacent mountains as their midpoint.
		const length = this.range.length;
		for (let i = 0; i < length - 1; i++) {
			const mountain = this.range[i];
			const nextMountain = this.range[i+1];

			const endX = ((mountain.x + nextMountain.x) / 2) - this.offset;
			const endY = (mountain.y + nextMountain.y) / 2;

			this.ctx.quadraticCurveTo(mountain.x - this.offset, mountain.y, endX, endY);

			if (endX > MountainConfig.screenWidth) {
				break;
			}
		}

		// Give the mountains a slight outline.  We make sure to stroke before filling because we don't
		// want to put a line on the bottom of the canvas.
		this.ctx.stroke();

		// Connect the right side of the mountain range to the ground.
		const lastMountain = this.range[length - 1];
		this.ctx.lineTo(lastMountain.x - this.offset + lastMountain.width, MountainConfig.screenHeight);

		// Color the mountain range.
		this.ctx.closePath();
		this.ctx.fill();
	}
}

function randomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

class MountainScreen {
	// Takes in a list of canvases to render mountain ranges on.
	constructor(canvases) {
		this.mountains = canvases.map((canvas, i) => new MountainRange(canvas, i));
		this.lastTime = performance.now();

		// The furthest back layer to redraw.
		// We start by drawing all mountains.
		this.maxLayer = this.mountains.length - 1;
	}

	redraw(timestamp) {
		// Render the scene, updating the position of the mountains to shift them left as time passes.
		// As an optimization, we draw each mountain range on its own canvas and only update them
		// when necessary.  This means we'll redraw the front range that moves the fastest every frame,
		// but will only redraw the slow-moving back range every 4 frames.

		// Determine how much time has passed
		const steps = (timestamp - this.lastTime) / 50;

		for (let i = 0; i <= this.maxLayer; i++) {
			this.mountains[i].update(steps);
			this.mountains[i].render();
		}

		// Update the per-frame information
		this.maxLayer = (this.maxLayer + 1) % this.mountains.length;
		this.lastTime = timestamp;

		requestAnimationFrame(timestamp => this.redraw(timestamp));
	}

	render() {
		requestAnimationFrame(timestamp => this.redraw(timestamp));
	}
}
