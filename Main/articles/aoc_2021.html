<meta property="og:description" content="Solving AoC'21 with Rust before Python can start"/>
<title>Solving Advent of Code 2021 with Rust before Python can start</title>
</head>
<body>
    <div id="main">
        <h1>Advent of Code 2021 in Rust before Python can start</h1>
        <h2></h2>
        <p>
		I did <a href="https://adventofcode.com/2021" target="_blank">Advent of Code</a> for the second time this year.  For those of you who haven't heard of it, it's a set of short programming puzzles that are released two at a time for the first 25 days of December.  People tend to have different goals when solving the problems &mdash; some people race to finish quickly and make the leaderboard, some try to learn new languages, and some do ridiculous things (like <a href="https://scratch.mit.edu/users/3j0hn/projects/" target="_blank">a person</a> who solved most of the puzzles in <a href="https://scratch.mit.edu/about" target="_blank">Scratch</a>!).  Rather than focusing on quickly submitting solutions, I tried to optimize the runtime of my solutions with an initial goal of solving all puzzles in 800ms (about the time it takes the <a href="https://en.wikipedia.org/wiki/Java_virtual_machine" target="_blank">JVM</a> to cold start).<br><br>

		As you may have guessed from this article's title, that goal was achieved - <b>I solved all 49 puzzles in ~65ms without multithreading on an AMD 5950x.</b>  This greatly exceeded my expectations and ended up being faster than even <tt>python</tt> can start (~70ms on the same machine)!  Additionally, three days (<a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day23/index.html" target="_blank">23</a>, <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day25/index.html" target="_blank">25</a>, and <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day15/index.html" target="_blank">15</a>) took the vast majority of that time.  Without those days, the remaining solutions run in a handful of milliseconds total.
		</p>

		<img src="aoc_2021/runtime_chart.svg" title="Chart of solution runtimes by day">

		<p>
		<i>Note: The title of this article is a bit clickbaity &mdash; with <tt>python</tt> not resident in the <a href="https://www.thomas-krenn.com/en/wiki/Linux_Page_Cache_Basics" target="_blank">page cache</a>, it takes the 5950x-based system ~70ms to run <tt>time python3 -c "exit()"</tt>, but it only takes ~10ms with a hot page cache.</i>
        </p>

        <h2>Show me the code!</h2>
        <p>
		I had the idea to try using <tt><a href="https://doc.rust-lang.org/rustdoc/what-is-rustdoc.html" target="_blank">rustdoc</a></tt> as a way to present my code along with explanations of what I found interesting about the problems and how I optimized them.  This format should allow you to browse through my solutions to problems, see the types I used and read my thoughts, and view the actual code I wrote.  It's the first time I've tried using <tt>rustdoc</tt> like this, so I'd appreciate feedback.<br><br>

		<b>You can find <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/">my writeups and solutions here</a>, and you should probably read them before continuing.</b>  You'll get the most value out of them if you've already solved AoC 2021, but you can also <a href="https://adventofcode.com/2021" target="_blank">follow along</a> with the problem descriptions for part one of each day.<br><br>

		Now that you've done that...
        </p>

		<h2>Optimizing code</h2>
		<p>
		An unexpected problem with writing efficient AoC solutions is the inability to effectively benchmark them, making it hard to make informed decisions on what to improve.  Many performance analysis tools use approaches similar to <tt><a href="https://perf.wiki.kernel.org/index.php/Tutorial" target="_blank">perf</a></tt>, which involve sampling the program runtime every few milliseconds (totally non-biased shoutout to <a href="https://functiontrace.com" target="_blank">FunctionTrace</a> as a great non-sampled Python profiler!).  As the slowest puzzle (<a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day23/index.html" target="_blank">day 23</a>) ran in ~31ms, it's hard to gather enough data to make optimization decisions off of.  As a result, I generally optimized based on a few heuristics and used <tt><a href="https://crates.io/crates/criterion" target="_blank">cargo-criterion</a></tt> to validate my hypotheses.<br><br>

		I used two main heuristics, which are things to always keep in mind when you care about performance:
		</p>
		<ol>
			<li>The fastest code is code that doesn't exist</li>
			<li>CPUs are fast, and memory is very slow</li>
		</ol>

		<p>
		My initial goal was always to find a faster algorithm that allowed me to avoid some computations.  For example, for <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day17/index.html" target="_blank">Day 17</a> Part 2, any optimization we do to computing steps of the projectile movement will still be slower than avoiding computing most of the steps via <a href="https://en.wikipedia.org/wiki/Triangular_number" target="_blank">triangle numbers</a>.  Note that this doesn't need to be a faster algorithm in order to do less work &mdash; writing code so the compiler can elide bounds checks, or memoizing the results of some computations can often be very effective.<br><br>

		After finding an efficient algorithm, I focused on minimizing the amount of time spend waiting on reads for main memory.  This is typically done via the <a href="https://en.wikipedia.org/wiki/CPU_cache" target="_blank">processor's cache</a>, which will transparently cache accesses to main memory.  However, caches have a fixed size that tends to be substantially smaller than the amount of RAM on a system &mdash; my development machine with 32GB of RAM only has ~1MB of cache!  To efficiently use the cache, it's important to minimize the size of data that's being used, as well as use patterns that can utilize the cache.<br><br>

		<a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day23/index.html" target="_blank">Day 23</a> has a good example of minimizing the size of data.  We need to represent many different game states and iterate through them, but a game naively consists of 27 different spaces each requiring a byte to store, which is then padded to 32 bytes by the compiler.  By removing unreachable states and compressing some information into bits, we're able to reduce this to 16 bytes, allowing us to double the number of game states we can fit in our cache before taking the long trip to main memory.  If we remove only this 16 byte optimization, day 23 is more than 30% slower!<br><br>

		While optimizing the size of data structures can help fit more data into the cache and allow the CPU to spend its time computing rather than waiting for memory, it's also important to ensure the program effectively uses the cache. To be feasible to implement in hardware, CPU caches store <i>lines</i> (typically 64 bytes each) rather than individual bytes.  This means that after you access a location in memory, accessing anything soon after it will likely hit the cache, avoiding a round-trip to memory.  As an example, we utilize this in <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day9/index.html" target="_blank">Day 9</a> by iterating over each row in order rather than using something like BFS.  As we're iterating over each <tt>u16</tt> (taking 2 bytes of space), we'll load 32 entries into the cache for memory access we do.  If we instead iterated over each column, we'd end up loading many different lines into the cache but not immediately using them, which would cause us to access main memory more often.  <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/day15/index.html" target="_blank">Day 15</a> is a good example of a case where we can't necessarily do this well; with Djikstra's the next neighbor may be located anywhere in the grid (512KB in memory), so we're rarely able to share memory accesses via the cache and instead spend most of our time waiting for data to load from main memory.
		</p>

		<h2>Thoughts on Rust</h2>
		<p>
		<b>I continue to really enjoy Rust.</b>  Many of my early solutions look fairly similar to the Python solutions, except with enforced error checking, no worries that they'll be wrong, and <i>blazingly fast</i> performance (it wouldn't be a Rust article if I didn't use this phrase somewhere).  By ensuring I'll rarely need to debug runtime errors and can still have native performance, Rust has become my favorite practical language.<br><br>

		Most of my solutions had two phases, where I first solved the problem without particularly caring about performance, then where I optimized it until it had satisfactory performance.
		During the first phase, I frequently used fancy data structures like <a href="https://doc.rust-lang.org/std/vec/struct.Vec.html" target="_blank">vectors</a> and <a href="https://doc.rust-lang.org/std/collections/struct.HashMap.html" target="_blank">hashtables</a>, then converted them to simpler data structures like fixed-sized arrays.  Rust's strong type system enabled me to refactor with confidence, knowing that I couldn't miss something and end up with annoying issues at runtime.<br><br>

		Performance-wise, it's really nice to have the ability to control memory-usage by specifically choosing my data types, while also not needing to manually manage memory via <tt>malloc()</tt> and <tt>free()</tt>.  I specifically chose integer sizes to optimize for memory layout and cache efficiency on many days, but almost never thought about allocations (and even without thought they only make up a tiny fraction of the <a href="https://github.com/flamegraph-rs/flamegraph" target="_blank">flamegraph</a> for my solutions).<br><br>

		<a href="https://doc.rust-lang.org/rust-by-example/scope/lifetime.html" target="_blank">Lifetimes</a> are one of the things that new Rust programmers seem to find confusing, but the Rust team has done a great job here,  I used references all over my code, but only needed to use explicit lifetime annotations twice.  The compiler does a much better job getting out of your way than in the pre-1.0 days.<br><br>

		I didn't need to touch <tt><a href="https://doc.rust-lang.org/nomicon/meet-safe-and-unsafe.html" target="_blank">unsafe</a></tt> at all, and from some quick profiling in <a href="https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler.html" target="_blank">VTune</a>, I don't feel like I missed any performance as a result.  This continues to reinforce my belief that the average programmer considering <tt>unsafe</tt> should consider refactoring their code to use a different pattern instead.<br><br>

		I didn't use <a href="https://doc.rust-lang.org/book/appendix-07-nightly-rust.html" target="_blank">nightly Rust</a> at all.  This mostly worked out, but there were some unstable APIs that I would've loved having.  Off the top of my head, <tt>BtreeMap::pop_first</tt> would've simplified my hacky priority queue implementation, and there were various unstable features related to <tt><a href="https://rust-lang.github.io/rfcs/2000-const-generics.html" target="_blank">const generics</a></tt> that I expected to exist.<br><br>

		<tt>Const generics</tt> seem like a neat idea, and it was my first time actually applying them to a Rust program.  I used them in a few places, but due to some limitations I never felt like they made a big improvement over passing an additional argument.  In particular, the inability to use <tt>const generics</tt> function arguments in data structures meant I couldn't make the optimizations around memory layout that I would've expected.<br><br>

		Integer operations seem to be horribly annoying in Rust.  If you care about the amount of memory your integers use, you'll end up casting back and forth between <tt>usize</tt> to index collections, <tt>isize</tt> to handle potentially negative cases, and whatever integral type you actually want.  This is a reasonable design decision, but it's incredibly frustrating to litter code with <tt>as usize</tt> and the like when I know the conversion will be safe.<br><br>

		<tt>rustdocs</tt> is amazing.  The ability to browse through generated documentation for everything in the ecosystem, including useful features like searching for functions by return type, makes onboarding to new libraries (or even the standard library) a breeze.  Something like <a href="https://hoogle.haskell.org/" target="_blank">Hoogle</a> would be still be nice to have, but I find Rust docs to be a massively more pleasant experience than the standard Python or Java docs.
		</p>

		<h2>A challenge appears</h2>
		<p>
		I thought using Rust to solve Advent of Code 2021 was enjoyable, and it was satisfying to write some high performance solutions with it.  I felt like I got initial solutions to the problems in a similar amount of time that a dynamic language like Python would've required (though optimization obviously took longer), and it's nice to feel like my CPU is being effectively utilized.<br><br>

		I'd love to see more people attempting to solve AoC with minimal runtime, and think a comparison with other high-performance languages like C or C++ would be fun.<br><br>

		From a quick scan of the <a href="https://www.reddit.com/r/adventofcode/" target="_blank">Advent of Code subreddit</a>, I believe I have the fastest public implementation for all days.  If anyone believes that's wrong (particularly if benchmarked on <a href="https://docs.rs/mbryant-aoc2021/latest/mbryant_aoc2021/benchmarks/trait.SystemInfo.html" target="_blank">one of my available systems</a>), let me know!
		</p>
