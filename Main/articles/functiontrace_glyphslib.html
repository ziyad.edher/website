<meta property="og:description" content="Optimizing Python with FunctionTrace"/>
<title>Quickly optimizing Python code with FunctionTrace</title>
</head>
<body>
    <div id="main">
        <h1>Quickly optimizing Python code with FunctionTrace</h1>
        <p>
        I recently did a quick optimization pass over <a href="https://github.com/googlefonts/glyphsLib">glyphsLib</a>, using <a href="https://functiontrace.com">FunctionTrace</a> to improve performance by ~30% in under half an hour.  This is writeup of my approach, which should be mostly extensible to optimizing other codebases.  Hopefully this writeup gives you a sense for how productive non-sampled profilers such as FunctionTrace can be when it comes to optimizing code, as well as exploring a new codebase.<br><br>

        <b>NOTE:</b> This article includes high-resolution images, and to get the most out of it you'll need to view them at their original size.  As such, it may not be an enjoyable experience on mobile.<br><br>

        A few important things before we start:
        <ul>
            <li>This exploration was driven by a bug report submitted for FunctionTrace where it OOMed on <a href="https://github.com/madig/noto-amalgamated">noto-amalgamated</a> after generating too large of a profile.  This led me to optimize for number of function calls rather than strictly performance.  Luckily these tend to be highly correlated in Python.</li>
            <li>I'd never seen or heard of this library before loading it in FunctionTrace.  As a result, I didn't try to make broader algorithmic changes, and I attempted to make small targeted changes that were likely to be correct rather than more significant refactors.</li>
            <li>This was a quick hack-and-slash optimization session.  No measurements were taken for intermediate optimizations, and my benchmark consisted of running the before/after code once each on an unloaded CPU via <tt>time</tt> &mdash; nothing involved with this process was rigorous!</li>
        </ul>
        </p>

        <h2>Backstory</h2>
        <p>
        <a href="https://functiontrace.com/">FunctionTrace</a> is a low-overhead non-sampled profiler for Python (which I've <a href="functiontrace_mozilla.html">written about before</a>) that uses the <a href="https://profiler.firefox.com/">Firefox Profiler</a> as a GUI.  Earlier this month I received a bug report saying that it was crashing with an out-of-memory error when run on <tt>noto-amalgamated</tt>, a project that uses <a href="https://github.com/googlefonts/noto-source/">Noto</a>.  After reproducing the issue, it became obvious that FunctionTrace on this tool generated too much data for the current version of the Firefox Profiler to handle.  While there are some interesting options for solving this in the future, I wanted to unblock the immediate use-case.  The majority of the profiler information FunctionTrace emits comes from recording information on every function call entry and exit, so if we could quickly reduce the number of function calls we'd be able to profile proportionally more of the program's execution.
        </p>

        <h2>Diving In</h2>

        <p>
        We've been given the following code to reproduce the issue:
        </p>
        <code><pre>
$ cd /tmp
$ git clone --depth 1 https://github.com/googlefonts/noto-source/
$ git clone https://github.com/madig/noto-amalgamated.git
$ cd noto-amalgamated && python3 -m venv venv && . venv/bin/activate && pip install glyphsLib==6.0.0b4
$ functiontrace amalgamate-noto.py</pre>
        </code>

        <p>
        Looking at <tt><a href="https://htop.dev/">htop</a></tt>, we can quickly see FunctionTrace consuming >10GB of RAM.  As the Firefox Profiler runs in a browser and therefore is limited to a 32-bit heap, we know that it won't be able to work with profiles larger than 4GB.  To profile this program, we'll need to only look at a short chunk of its execution.  We'll be conservative and start with 10s:
        </p>

        <code><pre>
$ timeout 10s functiontrace amalgamate-noto.py
...
[FunctionTrace] Wrote profile data to /tmp/noto-amalgamated-plain/functiontrace.json (1021.3 MB)</pre>
        </code>

        <p>
        That looks like it should fit quite nicely into a 32-bit heap!  Loading this into the Firefox Profiler gives us the following view:
        </p>

        <img src="functiontrace_glyphslib/opt1.png" title="Initial profile of the program">

        <h2>FunctionTrace 101</h2>
        <p>
        A FunctionTrace profile stores the execution history of a program over time.  For this article, we'll only be using the <i>Stack Chart</i> view, which allows you to easily explore that execution history.  Each rectangle in the view represents a function call that occurred during our program's execution, and the width specifies how long the function ran for.<br><br>
        The stack chart behaves like a tree, with functions higher up having called functions below it.  For example, <tt>exec()</tt> is the uppermost function, and as it was called to load the program and begin it's execution, it's the parent of all other functions.  We're typically not interested in functions in the middle (vertically) of the chart &mdash; functions near the top tend to have names that can inform us of what code is running, while functions at the bottom tend to be ripe for optimization.<br><br>
        If you'd like to better understand FunctionTrace, there's a live demo on <a href="https://functiontrace.com/">the FunctionTrace website</a> you can play around with.  Otherwise, let's dive in!
        </p>


        <p>
        Our initial view of the profile looks pretty repetitive.  We can see that there are repeated sections of <tt>load()</tt> calls followed by <tt>to_ufos()</tt>.  These probably cover different conceptual pieces of the program, so we can optimize <tt>load()</tt> and <tt>to_ufos()</tt> separately.
        </p>

        <h2>load()</h2>
        <p>
        We start by picking an instance of <tt>load()</tt> and looking at it, and quickly realize that it's also roughly composed of two repeating subsections:
        </p>

        <img src="functiontrace_glyphslib/opt2.png" title="Zoomed into load">

        <p>
        This function is spending lots of time in various <tt>deepcopy()</tt> calls, as well as in <tt>_parse_nodes_dict()</tt>.  It's probably hard to optimize <tt>deepcopy()</tt> usage without knowing more about the program, but let's look closer at <tt>_parse_nodes_dict()</tt>:
        </p>

        <img src="functiontrace_glyphslib/opt3.png" title="Zoomed into _parse_nodes_dict">

        <p>
        There's some initialization, then series of repeated <tt>&lt;lambda&gt;</tt>, <tt>__init__</tt>, <tt>read</tt>, and <tt>append</tt> calls.   This looks like a potential target, so we'll dive into the source code for this function, which is actually part of <tt>glyphsLib</tt>.
        </p>

        <code><pre>
def _parse_nodes_dict(self, parser, d):
    for x in d:
        if parser.format_version == 3:
            self.nodes.append(GSNode().read_v3(x))
        else:
            self.nodes.append(GSNode().read(x))</pre>
        </code>

        <p>
        That explains the sequence of <tt>append()</tt> calls!  A loop of <tt>append()</tt>s looks a lot like what <tt>extend()</tt> in the <a href="https://docs.python.org/3/library/stdtypes.html#typesseq-mutable">Python stdlib</a> does, so let's quickly replace that:
        </p>

        <code><pre>
def _parse_nodes_dict(self, parser, d):
    if parser.format_version == 3:
        <b>self.nodes.extend(</b>GSNode().read_v3(x)<b> for x in d)</b>
    else:
        <b>self.nodes.extend(</b>GSNode().read(x)<b> for x in d)</b></pre>
        </code>

        <p>
        Looking a little closer, we can also explain the <tt>&lt;lambda&gt;</tt> calls &mdash; <tt>self.nodes</tt> is a computed property:
        </p>

        <code><pre>
nodes = property(
    lambda self: PathNodesProxy(self),
    lambda self, value: PathNodesProxy(self).setter(value),
)</pre>
        </code>

        <p>
        This appears to be a common pattern in the <tt>glyphsLib</tt> codebase, and we'll get a few easy wins from removing it.  In this case, we'd expect it to no longer show up in our profile since we're not loading <tt>self.nodes</tt> in a loop anymore, and after profiling again we can see that our assumption is correct.
        </p>

        <img src="functiontrace_glyphslib/opt4.png">

        <h3>GSNode.read()</h3>

        <p>
        Now we're spending our time in a single <tt>extend()</tt> call that's repeatedly running <tt>read()</tt>, which spends a fair amount of time calling into <tt>parse_float_or_int()</tt>.  When we look at <tt>read()</tt>'s source, we can see that it must be doing something like parsing the input into <tt>(x, y)</tt> coordinates:
        </p>

        <code><pre>
def read(self, line):
    m = self._PLIST_VALUE_RE.match(line).groups()
    self.position = Point(parse_float_or_int(m[0]), parse_float_or_int(m[1]))
    self.type = m[2].lower()
    self.smooth = bool(m[3])

    if m[4] is not None and len(m[4]) > 0:
        value = self._decode_dict_as_string(m[4])
        parser = Parser()
        self._userData = parser.parse(value)

    return self</pre>
        </code>

        <p>
        This looks too much like interesting logic for me to try to change most of this, but <tt>parse_float_or_int()</tt> stood out in the profile.
        </p>

        <code><pre>
def parse_float_or_int(value_string):
    v = float(value_string)
    if v.is_integer():
        return int(v)
    return v</pre>
        </code>

        <p>
        That <tt>is_integer()</tt> call looks expensive!  Let's avoid it (and the <tt>float</tt> conversion) when possible by using exceptions instead.  This will probably be a bit more expensive if the input is a <tt>float</tt>, but should be significantly cheaper if it is an <tt>int</tt>, as well as removing a function call.
        </p>

        <code><pre>
def parse_float_or_int(value_string):
    <b>try:</b>
        return int(value_string)
    <b>except ValueError:</b>
        return float(value_string)</pre>
        </code>

        <p>
        Looking back at <tt>GSNode.read()</tt>, we'd now expect it to be pretty efficient.  But why does the profile show <tt>isinstance()</tt> calls?
        </p>

        <code><pre>
@property
def position(self):
    return self._position

@position.setter
def position(self, value):
    if not isinstance(value, Point):
        value = Point(value[0], value[1])
    self._position = value</pre>
        </code>

        <p>
        There are those pesky computed properties again.  Since we know <tt>value</tt> will be a <tt>Point</tt>, we can write to the underlying <tt>self._position</tt> instead:
        </p>

        <code><pre>
def read(self, line):
    ...
    <b>self._position</b> = Point(parse_float_or_int(m[0]), parse_float_or_int(m[1]))
    ...</pre>
        </code>

        <p>
        After applying these changes, the profile for <tt>load()</tt> looks much smoother:
        </p>

        <img src="functiontrace_glyphslib/opt5.png" title="Cleaned up view of load">

        <h2>to_ufos()</h2>
        <p>
        We now switch our attention to the other half of this program, <tt>to_ufos()</tt>.  It appears to be spending most of its time in <tt>to_ufo_glyph()</tt>.
        </p>

        <img src="functiontrace_glyphslib/opt6-wide.png" title="Overview of to_ufos">

        <p>
        The <tt>_ensureMasterLayers()</tt> function stands out as taking a long time, as well as repeatedly being called from <tt>to_ufo_glyph()</tt>.
        </p>

        <img src="functiontrace_glyphslib/opt6-zoom.png" title="Zoomed in on _ensureMasterLayers from the previous image">
        <p>
        From the instances of <tt>&lt;lambda&gt;</tt> in the profile, we could guess that it's spending lots of time looking up computed properties, and looking at the code confirms that.
        </p>

        <code><pre>
def _ensureMasterLayers(self):
    # Ensure existence of master-linked layers (even for iteration, len() etc.)
    # if accidentally deleted
    if not self._owner.parent:
        return
    for master in self._owner.parent.masters:
        if self._owner.parent.masters[master.id] is None:
            newLayer = GSLayer()
            newLayer.associatedMasterId = master.id
            newLayer.layerId = master.id
            self._owner._setupLayer(newLayer, master.id)
            self.__setitem__(master.id, newLayer)</pre>
        </code>

        <p>
        A quick skim makes me think <tt>self._owner.parent.masters</tt> doesn't need to be computed <i>this</i> often, so we can hoist it out of the loop, then generate a new profile.
        </p>

        <code><pre>
def _ensureMasterLayers(self):
    # Ensure existence of master-linked layers (even for iteration, len() etc.)
    # if accidentally deleted
    if not self._owner.parent:
        return
    <b>masters = self._owner.parent.masters</b>
    for master in <b>masters</b>:
        if <b>masters</b>[master.id] is None:
            newLayer = GSLayer()
            newLayer.associatedMasterId = master.id
            newLayer.layerId = master.id
            self._owner._setupLayer(newLayer, master.id)
            self.__setitem__(master.id, newLayer)</pre>
        </code>

        <br>
        <img src="functiontrace_glyphslib/opt7.png">

        <p>
        <tt>_ensureMasterLayers()</tt> now looks much smaller compared to the other function calls, and is itself making fewer function calls.<br><br>

        We've already made a substantial improvement on how busy the profile looks, and we're running out of obvious next steps.  It's time to benchmark!
        </p>

        <code><pre>
Before: python3 amalgamate-noto.py 1001.63s user 8.12s system 99% cpu 16:50.58 total
<b>After:  python3 amalgamate-noto.py  875.36s user 7.48s system 99% cpu 14:42.88 total</b></pre>
        </code>

        <p>
        Not bad, given that we only spent ~15 minutes, and changed 11 lines of code in a codebase we've never seen before!
        </p>

        <h2>Just a bit more</h2>
        <p>
        Normally I would call it quits here, but seeing a function like <tt>_ensureMasterLayers()</tt> being called so frequently (in every invocation of <tt>__next__()</tt>) gives me pause.  Looking at the codebase, we can see that it is called at the start of many other functions for its class (<tt>GlyphLayerProxy</tt>).  That's a pretty expensive way to ensure invariants which are likely already true!<br><br>

        It may not be safe to remove this function entirely, but there are likely workarounds.  We could elide this check in release builds, or only run it after we know invariants may have been broken.  Since the owner of the code <i>could</i> make an optimization like this, for now I'm happy to do a hacky version and remove <tt>_ensureMasterLayers</tt> from all of the functions but <tt>GlyphLayerProxy.append()</tt>.  Re-running our benchmark, we see that our hunch paid off.
        </p>

        <code><pre>
Before: python3 amalgamate-noto.py 1001.63s user 8.12s system 99% cpu 16:50.58 total
<b>After:  python3 amalgamate-noto.py  780.09s user 8.27s system 99% cpu 13:09.09 total</b></pre>
        </code>

        <p>
        Some of our changes might not be production-worthy yet, but our quick pass with a non-sampled profiler has at least shown that there's &gt;25% low-hanging fruit for the maintainer to pluck!
        </p>
