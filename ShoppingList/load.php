<?php

$contents = "list.json";

if (isset($_GET['first'])) {
	// If we're requesting for the first time, just serve the file.
	// After that, we'll only push the file on updates.
	echo file_get_contents($contents);
	return;
}

// Poll every 200ms
$poll_time = 200000;
$modified = filemtime($contents);


// Poll for 60s, as the webserver will typically kill the connection around
// then.
// NOTE: Don't use set_time_limit(), as that doesn't count syscalls,
// leading to php processes living forever.
for ($i = 0; $i < 60 * (1000 * 1000); $i += $poll_time) {
	if (filemtime($contents) != $modified) {
		// The file has been modified.
		break;
	}

	$modified = filemtime($contents);
	clearstatcache(true, $contents);
	usleep($poll_time);
}
echo file_get_contents($contents);

?>
